#include "Auto.h"



Auto::Auto()
{
	pocet_moznych_servisov = 0;
	pocet_servisov = 0;
}


Auto::~Auto()
{
}

Zakrok Auto::Povedz_mozny_servis(int i)
{
	return Mozny_servis[i];
}

int Auto::Povedz_pocet_servisov()
{
	return pocet_servisov;
}

void Auto::Nacitaj_jeden_zakrok(QString string,int cena)//pri vytvarani
{
	Zakrok novy;
	novy.Nastav_meno(string);
	novy.Nastav_cenu(cena);
	Vykonany_servis.append(novy);
}

void Auto::Nacitaj_jeden_mozny_zakrok(QString string, int cena)
{
	Zakrok novy;
	novy.Nastav_meno(string);
	novy.Nastav_cenu(cena);
	Mozny_servis.append(novy);
}

Zakrok Auto::Povedz_vykonany_servis(int i)
{
	return Vykonany_servis[i];
}

QString Auto::Povedz_meno()
{
	return meno;
}

void Auto::Nastav_meno(QString string)
{
	meno = string;
}

int Auto::Povedz_pocet_moznych_servisov()
{
	return pocet_moznych_servisov;
}

void Auto::Nastav_pocet_vykonanych_servisov()
{
	pocet_servisov = Vykonany_servis.size();
}

void Auto::Nastav_pocet_moznych_servisov()
{
	pocet_moznych_servisov = Mozny_servis.size();
}
