#pragma once

#include "Zakrok.h"

#include <QVector>
#include <QString>

class Auto
{
public:
	Auto();
	~Auto();
	Zakrok Povedz_mozny_servis(int i);
	int Povedz_pocet_servisov();//vykonanych
	void Nacitaj_jeden_zakrok(QString string,int cena);//pri vytvarani
	void Nacitaj_jeden_mozny_zakrok(QString string, int cena); // nacita mozny zakrok
	Zakrok Povedz_vykonany_servis(int i);
	QString Povedz_meno();
	void Nastav_meno(QString string);
	int Povedz_pocet_moznych_servisov();
	void Nastav_pocet_vykonanych_servisov();
	void Nastav_pocet_moznych_servisov();

private:
	QVector <Zakrok> Vykonany_servis;
	QVector <Zakrok> Mozny_servis;
	int pocet_servisov;
	int pocet_moznych_servisov;
	QString meno;
};

