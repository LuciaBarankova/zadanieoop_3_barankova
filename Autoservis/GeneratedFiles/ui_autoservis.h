/********************************************************************************
** Form generated from reading UI file 'autoservis.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTOSERVIS_H
#define UI_AUTOSERVIS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AutoservisClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_5;
    QFormLayout *formLayout;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *Prihlasenie;
    QPushButton *Objednavka;
    QPushButton *Vykonane_naAute;
    QSpacerItem *verticalSpacer_2;
    QPushButton *vymaz_pouzivatela;
    QPushButton *pridaj_pouzivatela;
    QVBoxLayout *verticalLayout_2;
    QPushButton *Odhlasenie;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label_4;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *Zoznam_Aut;
    QListWidget *Auta;
    QPushButton *pridaj_auto;
    QVBoxLayout *verticalLayout_4;
    QLabel *Zoznam_Zakrokov;
    QListWidget *Zakroky;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *AutoservisClass)
    {
        if (AutoservisClass->objectName().isEmpty())
            AutoservisClass->setObjectName(QStringLiteral("AutoservisClass"));
        AutoservisClass->resize(730, 280);
        centralWidget = new QWidget(AutoservisClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_5 = new QVBoxLayout(centralWidget);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, 0, -1);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(-1, -1, 0, -1);
        Prihlasenie = new QPushButton(centralWidget);
        Prihlasenie->setObjectName(QStringLiteral("Prihlasenie"));

        verticalLayout->addWidget(Prihlasenie);

        Objednavka = new QPushButton(centralWidget);
        Objednavka->setObjectName(QStringLiteral("Objednavka"));

        verticalLayout->addWidget(Objednavka);

        Vykonane_naAute = new QPushButton(centralWidget);
        Vykonane_naAute->setObjectName(QStringLiteral("Vykonane_naAute"));

        verticalLayout->addWidget(Vykonane_naAute);

        verticalSpacer_2 = new QSpacerItem(20, 100, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        vymaz_pouzivatela = new QPushButton(centralWidget);
        vymaz_pouzivatela->setObjectName(QStringLiteral("vymaz_pouzivatela"));
        vymaz_pouzivatela->setFlat(true);

        verticalLayout->addWidget(vymaz_pouzivatela);

        pridaj_pouzivatela = new QPushButton(centralWidget);
        pridaj_pouzivatela->setObjectName(QStringLiteral("pridaj_pouzivatela"));
        pridaj_pouzivatela->setAutoDefault(false);
        pridaj_pouzivatela->setFlat(true);

        verticalLayout->addWidget(pridaj_pouzivatela);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, -1, 0, -1);
        Odhlasenie = new QPushButton(centralWidget);
        Odhlasenie->setObjectName(QStringLiteral("Odhlasenie"));

        verticalLayout_2->addWidget(Odhlasenie);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_2->addWidget(label);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_2->addWidget(label_3);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_2->addWidget(label_4);

        verticalSpacer = new QSpacerItem(20, 100, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout_2);


        formLayout->setLayout(0, QFormLayout::LabelRole, horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, 0, -1);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(-1, -1, 0, -1);
        Zoznam_Aut = new QLabel(centralWidget);
        Zoznam_Aut->setObjectName(QStringLiteral("Zoznam_Aut"));

        verticalLayout_3->addWidget(Zoznam_Aut);

        Auta = new QListWidget(centralWidget);
        Auta->setObjectName(QStringLiteral("Auta"));
        Auta->setSelectionBehavior(QAbstractItemView::SelectRows);

        verticalLayout_3->addWidget(Auta);

        pridaj_auto = new QPushButton(centralWidget);
        pridaj_auto->setObjectName(QStringLiteral("pridaj_auto"));

        verticalLayout_3->addWidget(pridaj_auto);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(-1, 0, 0, -1);
        Zoznam_Zakrokov = new QLabel(centralWidget);
        Zoznam_Zakrokov->setObjectName(QStringLiteral("Zoznam_Zakrokov"));

        verticalLayout_4->addWidget(Zoznam_Zakrokov);

        Zakroky = new QListWidget(centralWidget);
        Zakroky->setObjectName(QStringLiteral("Zakroky"));
        Zakroky->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        Zakroky->setSelectionMode(QAbstractItemView::MultiSelection);

        verticalLayout_4->addWidget(Zakroky);


        horizontalLayout_2->addLayout(verticalLayout_4);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout_2);


        verticalLayout_5->addLayout(formLayout);

        AutoservisClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(AutoservisClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 730, 26));
        AutoservisClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(AutoservisClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        AutoservisClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(AutoservisClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        AutoservisClass->setStatusBar(statusBar);

        retranslateUi(AutoservisClass);
        QObject::connect(Prihlasenie, SIGNAL(clicked()), AutoservisClass, SLOT(otvor_login()));
        QObject::connect(Objednavka, SIGNAL(clicked()), AutoservisClass, SLOT(vytvor_objednavku()));
        QObject::connect(Odhlasenie, SIGNAL(clicked()), AutoservisClass, SLOT(odhlasenie()));
        QObject::connect(Vykonane_naAute, SIGNAL(clicked()), AutoservisClass, SLOT(vykonane_zakroky_naAute()));
        QObject::connect(Auta, SIGNAL(itemSelectionChanged()), AutoservisClass, SLOT(vypis_zakroky()));
        QObject::connect(pridaj_auto, SIGNAL(clicked()), AutoservisClass, SLOT(pridaj_nove_auto()));
        QObject::connect(pridaj_pouzivatela, SIGNAL(clicked()), AutoservisClass, SLOT(pridaj_noveho_pouzivatela()));
        QObject::connect(vymaz_pouzivatela, SIGNAL(clicked()), AutoservisClass, SLOT(vymaz_pouzivatela()));
        QObject::connect(Zakroky, SIGNAL(itemSelectionChanged()), Objednavka, SLOT(showMenu()));

        pridaj_pouzivatela->setDefault(false);


        QMetaObject::connectSlotsByName(AutoservisClass);
    } // setupUi

    void retranslateUi(QMainWindow *AutoservisClass)
    {
        AutoservisClass->setWindowTitle(QApplication::translate("AutoservisClass", "Autoservis", 0));
        Prihlasenie->setText(QApplication::translate("AutoservisClass", "Prihlasit", 0));
        Objednavka->setText(QApplication::translate("AutoservisClass", "Vytvorit objednavku", 0));
        Vykonane_naAute->setText(QApplication::translate("AutoservisClass", "Vykonane zakroky na aute", 0));
        vymaz_pouzivatela->setText(QApplication::translate("AutoservisClass", "Vymaz pouzivatela", 0));
        pridaj_pouzivatela->setText(QApplication::translate("AutoservisClass", "Pridaj pouzivatela", 0));
        Odhlasenie->setText(QApplication::translate("AutoservisClass", "Odhlasit", 0));
        label->setText(QApplication::translate("AutoservisClass", "Prihlaseny uzivatel:", 0));
        label_3->setText(QString());
        label_2->setText(QApplication::translate("AutoservisClass", "Pohlavie:", 0));
        label_4->setText(QString());
        Zoznam_Aut->setText(QApplication::translate("AutoservisClass", "Zoznam vasich aut", 0));
        pridaj_auto->setText(QApplication::translate("AutoservisClass", "Pridaj auto", 0));
        Zoznam_Zakrokov->setText(QApplication::translate("AutoservisClass", "Zoznam moznych zakrokov pre vybrane auto", 0));
    } // retranslateUi

};

namespace Ui {
    class AutoservisClass: public Ui_AutoservisClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTOSERVIS_H
