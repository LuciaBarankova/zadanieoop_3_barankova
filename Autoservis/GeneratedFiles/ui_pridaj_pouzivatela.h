/********************************************************************************
** Form generated from reading UI file 'pridaj_pouzivatela.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRIDAJ_POUZIVATELA_H
#define UI_PRIDAJ_POUZIVATELA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_pridaj_pouzivatela
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer_5;
    QLineEdit *Meno;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *Pohlavie;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_3;
    QLineEdit *Heslo;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *pridaj_pouzivatela)
    {
        if (pridaj_pouzivatela->objectName().isEmpty())
            pridaj_pouzivatela->setObjectName(QStringLiteral("pridaj_pouzivatela"));
        pridaj_pouzivatela->resize(406, 242);
        verticalLayout_2 = new QVBoxLayout(pridaj_pouzivatela);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, -1, -1);
        label = new QLabel(pridaj_pouzivatela);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);


        verticalLayout->addLayout(horizontalLayout);

        Meno = new QLineEdit(pridaj_pouzivatela);
        Meno->setObjectName(QStringLiteral("Meno"));

        verticalLayout->addWidget(Meno);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_2 = new QLabel(pridaj_pouzivatela);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_2);

        Pohlavie = new QLineEdit(pridaj_pouzivatela);
        Pohlavie->setObjectName(QStringLiteral("Pohlavie"));

        verticalLayout->addWidget(Pohlavie);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_3 = new QLabel(pridaj_pouzivatela);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_3);

        Heslo = new QLineEdit(pridaj_pouzivatela);
        Heslo->setObjectName(QStringLiteral("Heslo"));

        verticalLayout->addWidget(Heslo);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        pushButton = new QPushButton(pridaj_pouzivatela);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_4->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(pridaj_pouzivatela);
        QObject::connect(pushButton, SIGNAL(clicked()), pridaj_pouzivatela, SLOT(close()));

        QMetaObject::connectSlotsByName(pridaj_pouzivatela);
    } // setupUi

    void retranslateUi(QDialog *pridaj_pouzivatela)
    {
        pridaj_pouzivatela->setWindowTitle(QApplication::translate("pridaj_pouzivatela", "pridaj_pouzivatela", 0));
        label->setText(QApplication::translate("pridaj_pouzivatela", "Meno noveho zakaznika:", 0));
        label_2->setText(QApplication::translate("pridaj_pouzivatela", "Pohlavie noveho zakaznika:", 0));
        label_3->setText(QApplication::translate("pridaj_pouzivatela", "Heslo noveho zakaznika:", 0));
        pushButton->setText(QApplication::translate("pridaj_pouzivatela", "Pridat", 0));
    } // retranslateUi

};

namespace Ui {
    class pridaj_pouzivatela: public Ui_pridaj_pouzivatela {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRIDAJ_POUZIVATELA_H
