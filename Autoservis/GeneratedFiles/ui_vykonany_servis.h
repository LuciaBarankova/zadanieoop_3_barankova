/********************************************************************************
** Form generated from reading UI file 'vykonany_servis.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VYKONANY_SERVIS_H
#define UI_VYKONANY_SERVIS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Vykonany_servis
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_3;
    QPushButton *pushButton;

    void setupUi(QDialog *Vykonany_servis)
    {
        if (Vykonany_servis->objectName().isEmpty())
            Vykonany_servis->setObjectName(QStringLiteral("Vykonany_servis"));
        Vykonany_servis->resize(314, 219);
        verticalLayout_2 = new QVBoxLayout(Vykonany_servis);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        listWidget = new QListWidget(Vykonany_servis);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setSelectionMode(QAbstractItemView::NoSelection);

        verticalLayout->addWidget(listWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, 0, -1);
        label_2 = new QLabel(Vykonany_servis);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        label = new QLabel(Vykonany_servis);
        label->setObjectName(QStringLiteral("label"));
        label->setLayoutDirection(Qt::LeftToRight);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(label);

        label_3 = new QLabel(Vykonany_servis);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout->addWidget(label_3);


        verticalLayout->addLayout(horizontalLayout);

        pushButton = new QPushButton(Vykonany_servis);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(Vykonany_servis);
        QObject::connect(pushButton, SIGNAL(clicked()), Vykonany_servis, SLOT(close()));

        QMetaObject::connectSlotsByName(Vykonany_servis);
    } // setupUi

    void retranslateUi(QDialog *Vykonany_servis)
    {
        Vykonany_servis->setWindowTitle(QApplication::translate("Vykonany_servis", "Vykonany_servis", 0));
        label_2->setText(QApplication::translate("Vykonany_servis", "Cena servisu celkovo:", 0));
        label->setText(QApplication::translate("Vykonany_servis", "0 ", 0));
        label_3->setText(QApplication::translate("Vykonany_servis", "\342\202\254", 0));
        pushButton->setText(QApplication::translate("Vykonany_servis", "Ok", 0));
    } // retranslateUi

};

namespace Ui {
    class Vykonany_servis: public Ui_Vykonany_servis {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VYKONANY_SERVIS_H
