/********************************************************************************
** Form generated from reading UI file 'vymazanie_adminom.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VYMAZANIE_ADMINOM_H
#define UI_VYMAZANIE_ADMINOM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Vymazanie_adminom
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QLabel *label;
    QComboBox *comboBox;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QDialog *Vymazanie_adminom)
    {
        if (Vymazanie_adminom->objectName().isEmpty())
            Vymazanie_adminom->setObjectName(QStringLiteral("Vymazanie_adminom"));
        Vymazanie_adminom->resize(384, 120);
        verticalLayout_2 = new QVBoxLayout(Vymazanie_adminom);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        label = new QLabel(Vymazanie_adminom);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        comboBox = new QComboBox(Vymazanie_adminom);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton = new QPushButton(Vymazanie_adminom);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(Vymazanie_adminom);
        QObject::connect(pushButton, SIGNAL(clicked()), Vymazanie_adminom, SLOT(close()));

        QMetaObject::connectSlotsByName(Vymazanie_adminom);
    } // setupUi

    void retranslateUi(QDialog *Vymazanie_adminom)
    {
        Vymazanie_adminom->setWindowTitle(QApplication::translate("Vymazanie_adminom", "Vymazanie_adminom", 0));
        label->setText(QApplication::translate("Vymazanie_adminom", "Vyberte zo zoznau meno zakaznika, ktoreho chcete vymazat:", 0));
        pushButton->setText(QApplication::translate("Vymazanie_adminom", "Vymazat", 0));
    } // retranslateUi

};

namespace Ui {
    class Vymazanie_adminom: public Ui_Vymazanie_adminom {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VYMAZANIE_ADMINOM_H
