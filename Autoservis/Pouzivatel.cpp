#include "Pouzivatel.h"



Pouzivatel::Pouzivatel()
{
	pocet_aut = 0;
}


Pouzivatel::~Pouzivatel()
{
}

void Pouzivatel::Nastav_meno(QString string)
{
	meno = string;
}

void Pouzivatel::Nastav_heslo(QString string)
{
	heslo = string;
}

void Pouzivatel::Nastav_pohlavie(QString c)
{
	pohlavie = c;
}

void Pouzivatel::Nastav_auto(QString string)
{
	Auto novy;
	novy.Nastav_meno(string);
	auta.append(novy);
}

void Pouzivatel::Nastav_servis_auta(int i,QString string,int cena)
{
	auta[i].Nacitaj_jeden_zakrok(string, cena);
	auta[i].Nastav_pocet_vykonanych_servisov();
}

void Pouzivatel::Nastav_mozny_servis_auta(int i, QString string, int cena)
{
	auta[i].Nacitaj_jeden_mozny_zakrok(string, cena);
	auta[i].Nastav_pocet_moznych_servisov();
}

void Pouzivatel::Vymaz_auta()
{
	auta.clear();
}

QString Pouzivatel::Povedz_pohlavie()
{
	return pohlavie;
}

QString Pouzivatel::Povedz_meno()
{
	return meno;
}

QString Pouzivatel::Povedz_heslo()
{
	return heslo;
}

void Pouzivatel::Zvys_pocet_aut_o1()
{
	pocet_aut++;
}

Auto Pouzivatel::Povedz_auto(int i)
{
	return auta[i];
}

int Pouzivatel::Povedz_pocet_aut()
{
	return pocet_aut;
}
