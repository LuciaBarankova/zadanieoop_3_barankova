#pragma once

#include "Auto.h"

#include <QVector>

class Pouzivatel
{
public:
	Pouzivatel();
	~Pouzivatel();
	void Nastav_meno(QString string);
	void Nastav_heslo(QString string);
	void Nastav_pohlavie(QString c);
	void Nastav_auto(QString string);	//nove auto s menom string
	void Nastav_servis_auta(int i,QString string,int cena);			//vykonany servis
	void Nastav_mozny_servis_auta(int i,QString string, int cena);		//mozny servis
	void Vymaz_auta();
	QString Povedz_pohlavie();
	QString Povedz_meno();
	QString Povedz_heslo();
	
	void Zvys_pocet_aut_o1();
	Auto Povedz_auto(int i);
	int Povedz_pocet_aut();


private: 
	int pocet_aut;
	QString pohlavie;
	QString meno;
	QString heslo;
	QVector <Auto> auta;

};

