#pragma once

#include <QString>
class Zakrok
{
public:
	Zakrok();
	~Zakrok();
	int Povedz_cenu();
	QString Povedz_nazov();
	void Nastav_meno(QString string);
	void Nastav_cenu(int n);

private:
	int cena;
	QString meno;
};

