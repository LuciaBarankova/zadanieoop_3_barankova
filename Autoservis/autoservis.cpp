#include "autoservis.h"

Autoservis::Autoservis(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.Odhlasenie->setEnabled(false);
	ui.Objednavka->setEnabled(false);
	ui.Vykonane_naAute->setEnabled(false);
	ui.pridaj_auto->setEnabled(false);
	ui.pridaj_pouzivatela->setFlat(true);
	ui.vymaz_pouzivatela->setFlat(true);
	ui.pridaj_pouzivatela->setEnabled(false);
	ui.vymaz_pouzivatela->setEnabled(false);

	Nacitaj_vsetko();
}

Autoservis::~Autoservis()
{

}

void Autoservis::otvor_login()
{
	Login l;
	
	for (int i = 0; i < pocet_pouzivatelov; i++)
		l.Pridaj_doComboBoxu(zakaznici[i].Povedz_meno());

	l.exec();
	l.close();
	bool ok = true;
	QMessageBox msg;

	index = l.Povedz_index(); 

	if (l.Povedz_heslo() == zakaznici[index].Povedz_heslo())
		ok = true;
	else ok = false;
	if (ok == false)
	{
		msg.setText("Neuspesne prihlasenie, zadali ste zle heslo");
		msg.exec();
	}
	else
	{
		msg.setText("Uspesne ste sa prihlasili");
		msg.exec();
		ui.Prihlasenie->setEnabled(false);
		ui.Odhlasenie->setEnabled(true);
		ui.pridaj_auto->setEnabled(true);
		Nastav_meno_pohlavie_prihlaseneho();
		if (zakaznici[index].Povedz_meno() == "ADMIN")		
			Bud_admin();
		else Vypis_zoznam_aut();
	}
}

void Autoservis::Nacita_Pouzivatelov()
{
	QFile file("Pouzivatelia.txt");
	Pouzivatel pom;
	zakaznici.clear();
	if (file.open(QFile::ReadOnly))
	{
		QTextStream in(&file);
		while (!in.atEnd())
		{
			pom.Nastav_meno(in.readLine());
			pom.Nastav_pohlavie(in.readLine());
			pom.Nastav_heslo(in.readLine());
			zakaznici.push_back(pom);
		}
		pocet_pouzivatelov = zakaznici.length();
	}
	file.close();
}

void Autoservis::Nacita_Cennik()	//tu nacitava vsetky mozne zakroky
{
	int i = 0;
	zakroky.clear();
	QFile file("Servis a cennik.txt");
	Zakrok pom;

	if (file.open(QFile::ReadOnly))
	{
		QTextStream in(&file);
		while (!in.atEnd())
		{
			pom.Nastav_meno(in.readLine());
			pom.Nastav_cenu((in.readLine()).toInt());
			zakroky.push_back(pom);
		}
		pocet_moznych_zakrokov = zakroky.length();
	}
	file.close();
}

void Autoservis::Nacita_Servis()		//tu nacitava konkretne ku kazdemu autu
{
	int i = 0;
	QFile file("Pouzivatelia Auta Servis.txt");
	QString b;
	bool t = false;
	QStringList list,list2;

	if (file.open(QFile::ReadOnly))
	{
		QTextStream in(&file);
		while (!in.atEnd())
		{
			for (int j = 0; j < zakaznici[i].Povedz_pocet_aut(); j++)
			{
				b = in.readLine();
				list = b.split(':');
				for (int k = 2; k < list.length(); k++)
				{
					list2 = list[k].split(';');
					zakaznici[i].Nastav_servis_auta(j, list2[0], list2[1].toInt());
				}
			}
			i++;
		}
		for (int i = 0; i < pocet_pouzivatelov; i++)
		{
			for (int j = 0; j < zakaznici[i].Povedz_pocet_aut(); j++)
			{
				for (int k = 0; k < pocet_moznych_zakrokov; k++)
				{
					t = false;
					for (int l = 0; l < zakaznici[i].Povedz_auto(j).Povedz_pocet_servisov(); l++)
					{
						if (zakroky[k].Povedz_nazov() == ((zakaznici[i].Povedz_auto(j)).Povedz_vykonany_servis(l)).Povedz_nazov())
						{
							t = true;
							break;
						}
					}
					if (t == false)
						zakaznici[i].Nastav_mozny_servis_auta(j, zakroky[k].Povedz_nazov(), zakroky[k].Povedz_cenu());
				}
			}
		}
	}
	file.close();
}

void Autoservis::Nacita_Auta()
{
	int i = 0;
	QString b = "";
	QStringList list;
	QFile file("Pouzivatelia Auta.txt");
	if (file.open(QFile::ReadOnly))
	{
		QTextStream in(&file);
		while (!in.atEnd())
		{
			b = in.readLine();
			list = b.split(':');
			for (int j = 1; j < list.length(); j++)
			{
				zakaznici[i].Nastav_auto(list[j]);
				zakaznici[i].Zvys_pocet_aut_o1();
			}
			i++;
		}
	}
	file.close();
}

void Autoservis::Nacitaj_vsetko()
{
	Nacita_Pouzivatelov();
	Nacita_Cennik();
	Nacita_Auta();
	Nacita_Servis();
}

void Autoservis::Nastav_meno_pohlavie_prihlaseneho()
{
	ui.label_3->setText(zakaznici[index].Povedz_meno());
	ui.label_4->setText(zakaznici[index].Povedz_pohlavie());
}

void Autoservis::Bud_admin()
{
	ui.pridaj_pouzivatela->setFlat(false);
	ui.vymaz_pouzivatela->setFlat(false);
	ui.pridaj_pouzivatela->setEnabled(true);
	ui.vymaz_pouzivatela->setEnabled(true);
	ui.pridaj_auto->setEnabled(false);
}

void Autoservis::vytvor_objednavku()
{
	//zapis do suboru OBJEDNAVKA
	QFile output("Objednavka.txt");
	if (output.open(QFile::Append | QFile::Text | QFile::Truncate))
	{
		QTextStream out(&output);
		QString nadpis = "Objednavka";
		QList <QListWidgetItem*> vybrane;
		int celkova_cena = 0;
		vybrane = ui.Zakroky->selectedItems();
		QVector <int> cena_zakroku;
		cena_zakroku.resize(vybrane.size());

		for (int i = 0; i < pocet_moznych_zakrokov; i++)
		{
			for (int j = 0; j < vybrane.size(); j++)
			{
				if (zakroky[i].Povedz_nazov() == vybrane[j]->text())
				{
					cena_zakroku[i]=zakroky[i].Povedz_cenu();
					celkova_cena += cena_zakroku[i];
					continue;
				}
			}
		}
		out << "Objednavka servisu na auto:" << endl << ui.Auta->currentItem()->text() << endl << endl << "Objednany servis:" << endl;
		for (int i = 0; i < vybrane.size(); i++)
		{
			out << i + 1 << ". " << vybrane[i]->text() << endl;
		}
		out << "Celkova cena za servis:\t" << celkova_cena << endl;
		output.close();

		//zapis do suboru s vykonanym servisom na aute
	/*	QFile input("Pouzivatelia Auta Servis.txt");
		if (input.open(QFile::Append | QFile::Text))
		{
			int pocet_aut_predchadzajucich = 0;
			QTextStream in(&input);
			for (int i = 0; i < index; i++)
			{
				pocet_aut_predchadzajucich += zakaznici[i].Povedz_pocet_aut();
			}
			for (int i = 0; i < pocet_aut_predchadzajucich; i++)
			{
				QString line = in.readLine();
			}
			for (int i = 0; i < vybrane.size(); i++)
			{
				in << ":" << vybrane[i]->text() << ";" << cena_zakroku[i];
			}
			input.close();
		}*/
	}
	//tu este musim vymazat tie zakroky, resp pridat do vykonanych, ktore su v objednavke
	output.close();

	QList <QListWidgetItem*> list = ui.Zakroky->selectedItems();
	QString vybrane_a = (ui.Auta->currentItem())->text();
	int vybrane_a_index = ui.Auta->currentRow();
	QFile servis_vymazat_vykonane("Pouzivatelia Auta Servis.txt");
	if (servis_vymazat_vykonane.open(QFile::Append | QFile::Text | QFile::Truncate))
	{
		QTextStream s(&servis_vymazat_vykonane);
		QVector <int> cena_;
		for (int i = 0; i < pocet_moznych_zakrokov; i++)
		{
			for (int j = 0; j < list.size(); j++)
			{
				if (zakroky[i].Povedz_nazov() == list[j]->text())
				{
					cena_.append(zakroky[i].Povedz_cenu());
					break;
				}
			}
		}
		bool t = false;
		for (int i = 0; i < list.size(); i++)
			zakaznici[index].Nastav_servis_auta(vybrane_a_index, list[i]->text(), cena_[i]);
		for (int i = 0; i < pocet_pouzivatelov - 1; i++)
		{
			for (int j = 0; j < zakaznici[i].Povedz_pocet_aut(); j++)
			{
				s << zakaznici[i].Povedz_meno() << ":" << zakaznici[i].Povedz_auto(j).Povedz_meno();
				for (int k = 0; k < zakaznici[i].Povedz_auto(j).Povedz_pocet_servisov(); k++)
				{
					s << ":" << zakaznici[i].Povedz_auto(j).Povedz_vykonany_servis(k).Povedz_nazov() << ";" << zakaznici[i].Povedz_auto(j).Povedz_vykonany_servis(k).Povedz_cenu();
				}
				s << endl;
			}
		}
	}
	servis_vymazat_vykonane.close();
	Nacitaj_vsetko();
	vypis_zakroky();
}

void Autoservis::vypis_zakroky()
{
	//vypise co je mozne na konkretnom aute vykonat
	ui.Zakroky->clear();
	ui.Vykonane_naAute->setEnabled(true);
	ui.Objednavka->setEnabled(true);
	int j = ui.Auta->currentRow();
	ui.Zakroky->clear();

	for (int i = 0; i < (zakaznici[index].Povedz_auto(j)).Povedz_pocet_moznych_servisov(); i++)
	{
		ui.Zakroky->addItem((zakaznici[index].Povedz_auto(j)).Povedz_mozny_servis(i).Povedz_nazov());
	}
}

void Autoservis::odhlasenie()
{
	ui.Auta->clear();
	ui.pridaj_pouzivatela->setFlat(true);
	ui.vymaz_pouzivatela->setFlat(true);
	ui.pridaj_pouzivatela->setEnabled(false);
	ui.vymaz_pouzivatela->setEnabled(false);
	ui.Prihlasenie->setEnabled(true);
	ui.Odhlasenie->setEnabled(false);
	ui.Objednavka->setEnabled(false);
	ui.Vykonane_naAute->setEnabled(false);
	ui.pridaj_auto->setEnabled(false);
	ui.Zakroky->clear();
	ui.label_3->clear();
	ui.label_4->clear();
}

void Autoservis::vykonane_zakroky_naAute()
{
	Vykonany_servis w;
	int suma = 0;
	for (int i = 0; i < zakaznici[index].Povedz_auto(ui.Auta->currentRow()).Povedz_pocet_servisov(); i++)
	{
		w.Nacitaj_doListWidget(zakaznici[index].Povedz_auto(ui.Auta->currentRow()).Povedz_vykonany_servis(i).Povedz_nazov());
		suma += zakaznici[index].Povedz_auto(ui.Auta->currentRow()).Povedz_vykonany_servis(i).Povedz_cenu();
	}
	w.Nacitaj_cenu(suma);
	w.exec();
}

void Autoservis::pridaj_nove_auto()
{
	Pridaj_auto w;
	w.exec();
	QString nove = w.povedz_meno();
	bool ok = w.zmenene();
	w.close();
	if (ok)
	{
		QFile auta_pridat("Pouzivatelia Auta.txt");
		if (auta_pridat.open(QFile::Append | QFile::Text | QFile::Truncate))
		{
			QTextStream a(&auta_pridat);
			zakaznici[index].Nastav_auto(nove);
			zakaznici[index].Zvys_pocet_aut_o1();
			for (int i = 0; i < pocet_pouzivatelov - 1; i++)
			{
				a << zakaznici[i].Povedz_meno();
				for (int j = 0; j < zakaznici[i].Povedz_pocet_aut(); j++)
				{
					a << ":" << zakaznici[i].Povedz_auto(j).Povedz_meno();
				}
				a << endl;
			}
		}
		QFile servis_pridat("Pouzivatelia Auta Servis.txt");
		if (servis_pridat.open(QFile::Append | QFile::Text | QFile::Truncate))
		{
			QTextStream s(&servis_pridat);
			for (int i = 0; i < pocet_pouzivatelov - 1; i++)
			{
				for (int j = 0; j < zakaznici[i].Povedz_pocet_aut(); j++)
				{
					s << zakaznici[i].Povedz_meno() << ":" << zakaznici[i].Povedz_auto(j).Povedz_meno();
					if (i == index && zakaznici[i].Povedz_auto(j).Povedz_meno() == nove)
					{
						s << endl;
						continue;
					}
					for (int k = 0; k < zakaznici[i].Povedz_auto(j).Povedz_pocet_servisov(); k++)
					{
						s << ":" << zakaznici[i].Povedz_auto(j).Povedz_vykonany_servis(k).Povedz_nazov() << ";" << zakaznici[i].Povedz_auto(j).Povedz_vykonany_servis(k).Povedz_cenu();
					}
					s << endl;

				}
			}
		}
		auta_pridat.close();
		servis_pridat.close();
		Nacitaj_vsetko();
		Vypis_zoznam_aut();
	}
}

void Autoservis::pridaj_noveho_pouzivatela()
{
	pridaj_pouzivatela w;
	w.exec();
	QString nove_m = w.Povedz_meno_novy();
	QString nove_p = w.Povedz_pohlavie_novy();
	QString nove_h = w.Povedz_heslo_novy();
	w.close();

	QFile pouzivatelia("Pouzivatelia.txt");
	if (pouzivatelia.open(QFile::Append | QFile::Text | QFile::Truncate))
	{
		QTextStream p(&pouzivatelia);
		//p << endl << nove_m << endl << nove_p << endl << nove_h << endl;
		for (int i = 0; i < pocet_pouzivatelov; i++)
		{
			if (i == pocet_pouzivatelov - 1)
				p << nove_m << endl << nove_p << endl << nove_h << endl;
			p << zakaznici[i].Povedz_meno() << endl << zakaznici[i].Povedz_pohlavie() << endl << zakaznici[i].Povedz_heslo() << endl;
		}
	}
	QFile auta_("Pouzivatelia Auta.txt");
	if (auta_.open(QFile::Append | QFile::Text))
	{
		QTextStream a(&auta_);
		a << nove_m << endl;
	}
	pouzivatelia.close();
	auta_.close();
	Nacitaj_vsetko();
}

void Autoservis::vymaz_pouzivatela()
{
	Vymazanie_adminom w;
	w.Nastav_comboBox("Nevymazat ziadneho zakaznika");
	for (int i = 0; i < pocet_pouzivatelov - 1; i++)
		w.Nastav_comboBox(zakaznici[i].Povedz_meno());
	w.exec();
	w.close();
	int v = w.Povedz_index() - 1;
	if (v != -1)
	{
		QString meno = zakaznici[v].Povedz_meno();

		QFile pouzivatelia("Pouzivatelia.txt");
		if (pouzivatelia.open(QFile::Append | QFile::Text | QFile::Truncate))
		{
			QTextStream p(&pouzivatelia);
			for (int i = 0; i < pocet_pouzivatelov; i++)
			{
				if (zakaznici[i].Povedz_meno() == meno) continue;
				p << zakaznici[i].Povedz_meno() << endl << zakaznici[i].Povedz_pohlavie() << endl << zakaznici[i].Povedz_heslo() << endl;
			}
		}
		QFile auta_vymazat("Pouzivatelia Auta.txt");
		if (auta_vymazat.open(QFile::Append | QFile::Text | QFile::Truncate))
		{
			QTextStream a(&auta_vymazat);
			for (int i = 0; i < pocet_pouzivatelov - 1; i++)
			{
				if (zakaznici[i].Povedz_meno() == meno) continue;
				a << zakaznici[i].Povedz_meno();
				for (int j = 0; j < zakaznici[i].Povedz_pocet_aut(); j++)
				{
					a << ":" << zakaznici[i].Povedz_auto(j).Povedz_meno();
				}
				a << endl;
			}
		}
		QFile servis_vymazat("Pouzivatelia Auta Servis.txt");
		if (servis_vymazat.open(QFile::Append | QFile::Text | QFile::Truncate))
		{
			QTextStream s(&servis_vymazat);
			for (int i = 0; i < pocet_pouzivatelov - 1; i++)
			{
				if (zakaznici[i].Povedz_meno() == meno)	continue;
				for (int j = 0; j < zakaznici[i].Povedz_pocet_aut(); j++)
				{
					s << zakaznici[i].Povedz_meno() << ":" << zakaznici[i].Povedz_auto(j).Povedz_meno();
					for (int k = 0; k < zakaznici[i].Povedz_auto(j).Povedz_pocet_servisov(); k++)
					{
						s << ":" << zakaznici[i].Povedz_auto(j).Povedz_vykonany_servis(k).Povedz_nazov() << ";" << zakaznici[i].Povedz_auto(j).Povedz_vykonany_servis(k).Povedz_cenu();
					}
					s << endl;
				}
			}
		}
		pouzivatelia.close();
		auta_vymazat.close();
		servis_vymazat.close();
		Nacitaj_vsetko();
	}
}

void Autoservis::Vypis_zoznam_aut()
{
	ui.Auta->clear();
	for (int i = 0; i < (zakaznici[index]).Povedz_pocet_aut(); i++)
	{
		ui.Auta->addItem((zakaznici[index].Povedz_auto(i)).Povedz_meno());
	}
}