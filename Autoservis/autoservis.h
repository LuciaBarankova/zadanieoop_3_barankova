#ifndef AUTOSERVIS_H
#define AUTOSERVIS_H

#include <QtWidgets/QMainWindow>
#include "ui_autoservis.h"
#include <login.h>
#include "Pouzivatel.h"
#include "Zakrok.h"
#include "Auto.h"
#include <vykonany_servis.h>
#include <vymazanie_adminom.h>
#include <pridaj_auto.h>
#include <pridaj_pouzivatela.h>

#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QChar>
#include <QVector>
#include <QLabel>
#include <QMessageBox>

class Autoservis : public QMainWindow
{
	Q_OBJECT

public:
	Autoservis(QWidget *parent = 0);
	~Autoservis();

	void Nacita_Pouzivatelov();
	void Nacita_Cennik();
	void Nacita_Servis();
	void Nacita_Auta();
	void Nacitaj_vsetko();

	void Vypis_zoznam_aut();
	void Nastav_meno_pohlavie_prihlaseneho();
	void Bud_admin();

	public slots:
	void otvor_login();
	void vytvor_objednavku();
	void vypis_zakroky();
	void odhlasenie();
	void vykonane_zakroky_naAute();
	void pridaj_nove_auto();
	void pridaj_noveho_pouzivatela();
	void vymaz_pouzivatela();

private:
	Ui::AutoservisClass ui;

	int index;
	int pocet_pouzivatelov;
	int pocet_moznych_zakrokov;
	QVector <Pouzivatel> zakaznici;
	QVector <Zakrok> zakroky;
};

#endif // AUTOSERVIS_H
