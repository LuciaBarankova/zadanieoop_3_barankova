﻿#include "login.h"

Login::Login(QWidget * parent) : QDialog(parent) 
{
	ui.setupUi(this);
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
	ui.comboBox->clear();
	ui.comboBox->currentIndexChanged(-1);
}

Login::~Login() 
{
	
}

QString Login::Povedz_meno()
{
	return meno;
}

QString Login::Povedz_heslo()
{
	return heslo;
}

int Login::Povedz_index()
{
	return index;
}

void Login::Pridaj_doComboBoxu(QString string)
{
	ui.comboBox->addItem(string);
}


void Login::prihlasit()
{
	index = ui.comboBox->currentIndex();
	meno = ui.comboBox->currentText();
	heslo = ui.lineEdit_2->text();
	close();
}