﻿#pragma once
#include <QDialog>
#include "ui_login.h"

#include <QLineEdit>
#include <QComboBox>
#include <QMessageBox>

class Login : public QDialog {
	Q_OBJECT

public:
	Login(QWidget * parent = Q_NULLPTR);
	~Login();
	QString Povedz_meno();
	QString Povedz_heslo();
	int Povedz_index();
	void Pridaj_doComboBoxu(QString string);

	public slots:
	void prihlasit();

private:
	Ui::Login ui;
	QString meno;
	QString heslo;
	int index;
};
