﻿#include "pridaj_auto.h"

Pridaj_auto::Pridaj_auto(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);
}

Pridaj_auto::~Pridaj_auto() {
	
}

QString Pridaj_auto::povedz_meno()
{
	return ui.lineEdit->text();
}

bool Pridaj_auto::zmenene()
{
	return ui.lineEdit->isModified();
}
