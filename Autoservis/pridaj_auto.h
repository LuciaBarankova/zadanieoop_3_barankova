﻿#pragma once
#include <QDialog>
#include "ui_pridaj_auto.h"
#include <QString>

class Pridaj_auto : public QDialog {
	Q_OBJECT

public:
	Pridaj_auto(QWidget * parent = Q_NULLPTR);
	~Pridaj_auto();
	QString povedz_meno();
	bool zmenene();

private:
	Ui::Pridaj_auto ui;
};
