﻿#include "pridaj_pouzivatela.h"

pridaj_pouzivatela::pridaj_pouzivatela(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);
}

pridaj_pouzivatela::~pridaj_pouzivatela() {
	
}

QString pridaj_pouzivatela::Povedz_meno_novy()
{
	return ui.Meno->text();
}

QString pridaj_pouzivatela::Povedz_pohlavie_novy()
{
	return ui.Pohlavie->text();
}

QString pridaj_pouzivatela::Povedz_heslo_novy()
{
	return ui.Heslo->text();
}
