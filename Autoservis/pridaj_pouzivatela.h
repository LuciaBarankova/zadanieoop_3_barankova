﻿#pragma once
#include <QDialog>
#include "ui_pridaj_pouzivatela.h"
#include <QString>

class pridaj_pouzivatela : public QDialog {
	Q_OBJECT

public:
	pridaj_pouzivatela(QWidget * parent = Q_NULLPTR);
	~pridaj_pouzivatela();
	QString Povedz_meno_novy();
	QString Povedz_pohlavie_novy();
	QString Povedz_heslo_novy();

private:
	Ui::pridaj_pouzivatela ui;
};
