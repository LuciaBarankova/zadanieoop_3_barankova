﻿#include "vykonany_servis.h"

Vykonany_servis::Vykonany_servis(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);	
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

}

Vykonany_servis::~Vykonany_servis() {
	
}

void Vykonany_servis::Nacitaj_doListWidget(QString string)
{
	ui.listWidget->addItem(string);
}

void Vykonany_servis::Nacitaj_cenu(int cena)
{
	ui.label->setText(QString::number(cena));
}
