﻿#pragma once
#include <QDialog>
#include "ui_vykonany_servis.h"

class Vykonany_servis : public QDialog {
	Q_OBJECT

public:
	Vykonany_servis(QWidget * parent = Q_NULLPTR);
	~Vykonany_servis();
	void Nacitaj_doListWidget(QString string);
	void Nacitaj_cenu(int cena);

private:
	Ui::Vykonany_servis ui;
};
