﻿#include "vymazanie_adminom.h"

Vymazanie_adminom::Vymazanie_adminom(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);
	ui.comboBox->clear();
}

Vymazanie_adminom::~Vymazanie_adminom() {
	
}

int Vymazanie_adminom::Povedz_index()
{
	return ui.comboBox->currentIndex();
}

void Vymazanie_adminom::Nastav_comboBox(QString string)
{
	ui.comboBox->addItem(string);
}
