﻿#pragma once
#include <QDialog>
#include "ui_vymazanie_adminom.h"
#include <QString>

class Vymazanie_adminom : public QDialog {
	Q_OBJECT

public:
	Vymazanie_adminom(QWidget * parent = Q_NULLPTR);
	~Vymazanie_adminom();
	int Povedz_index();
	void Nastav_comboBox(QString string);

private:
	Ui::Vymazanie_adminom ui;
};
